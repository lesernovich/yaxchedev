package domain;

import java.time.*;

public class Frutas {

    private int id;
    private int clave;
    private String nombre;
    private int precio;
    private boolean estado;
    private LocalDateTime fechaRegistro;
    private LocalDate ultimaModificacion;

    public Frutas() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public LocalDate getUltimaModificacion() {
        return ultimaModificacion;
    }

    public void setUltimaModificacion(LocalDate ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    @Override
    public String toString() {
        return "Frutas{" + "id=" + id + ", clave=" + clave + ", nombre=" + nombre + ", precio=" + precio + ", estado=" + estado + ", fechaRegistro=" + fechaRegistro + ", ultimaModificacion=" + ultimaModificacion + '}';
    }
}