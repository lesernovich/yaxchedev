package domain;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Agregar extends javax.swing.JFrame {

    static LinkedList<Frutas> frutas = new LinkedList<>();

    public LinkedList<Frutas> getEmpleados() {
        return frutas;
    }

    /*
     * Se crea una lista donde se van a ir guardando nuestros objetos (frutas).
     */

    public int generarID() {

        var x = LocalDateTime.now();
        int extraer = x.getNano() + x.getSecond();
        return extraer;

    }

    /*
     * Se crea un metodo para generar IDs (se va a usar para los id).
     */
    public static void guardarArchivo(File archivo) {
        FileWriter fichero = null;
        PrintWriter pw = null;

        try {
            fichero = new FileWriter(archivo);
            pw = new PrintWriter(fichero);

            for (Frutas e : frutas) {
                String linea = e.getId() + "," + e.getClave() + "," + e.getNombre() + "," + e.getPrecio() + "," + e.isEstado() + "," + e.getFechaRegistro() + "," + e.getUltimaModificacion();
                pw.println(linea);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fichero != null) {
                    fichero.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * Se crea un metodo para guardar el archivo
     */
    public Agregar() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        clave = new javax.swing.JTextField();
        nombre = new javax.swing.JTextField();
        precio = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Insertar = new javax.swing.JButton();
        aceptar = new javax.swing.JButton();
        obligar1 = new javax.swing.JLabel();
        obligar2 = new javax.swing.JLabel();
        obligar3 = new javax.swing.JLabel();
        fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Recaudería");
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(clave, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 120, 190, -1));
        getContentPane().add(nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 160, 190, -1));
        getContentPane().add(precio, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 200, 190, -1));

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel1.setText("Clave");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 120, -1, -1));

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel2.setText("Precio");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 200, -1, -1));

        jLabel3.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel3.setText("Nombre");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 160, -1, -1));

        Insertar.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        Insertar.setText("Insertar");
        Insertar.setContentAreaFilled(false);
        Insertar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Insertar.setFocusPainted(false);
        Insertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InsertarActionPerformed(evt);
            }
        });
        getContentPane().add(Insertar, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 280, -1, -1));

        aceptar.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        aceptar.setText("Aceptar");
        aceptar.setContentAreaFilled(false);
        aceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        aceptar.setFocusPainted(false);
        aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aceptarActionPerformed(evt);
            }
        });
        getContentPane().add(aceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 320, -1, -1));

        obligar1.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        getContentPane().add(obligar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, 130, 20));

        obligar2.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        getContentPane().add(obligar2, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 160, 130, 20));

        obligar3.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        getContentPane().add(obligar3, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, 130, 20));

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Imagenes/fondo1.jpg"))); // NOI18N
        getContentPane().add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 510));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void InsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InsertarActionPerformed

        boolean oblig1, oblig2, oblig3;
        String texto = clave.getText();

        texto = texto.replaceAll(" ", "");
        if (texto.length() == 0) {
            obligar1.setText("Obligatorio");
            oblig1 = false;
        } else {
            obligar1.setText(null);
            oblig1 = true;
        }

        texto = nombre.getText();
        texto = texto.replaceAll(" ", "");
        if (texto.length() == 0) {
            obligar2.setText("Obligatorio");
            oblig2 = false;
        } else {
            obligar2.setText(null);
            oblig2 = true;
        }

        texto = precio.getText();
        texto = texto.replaceAll(" ", "");
        if (texto.length() == 0) {
            obligar3.setText("Obligatorio");
            oblig3 = false;
        } else {
            obligar3.setText(null);
            oblig3 = true;
        }

        if (oblig1 == true && oblig2 == true && oblig3 == true) {

            Frutas fruta = new Frutas();

            fruta.setId(generarID());
            fruta.setClave(Integer.parseInt(clave.getText()));
            fruta.setNombre(nombre.getText());
            fruta.setPrecio(Integer.parseInt(precio.getText()));
            fruta.setEstado(true);
            fruta.setFechaRegistro(LocalDateTime.now());
            fruta.setUltimaModificacion(LocalDate.now());

            frutas.add(fruta);

            clave.setText(null);
            nombre.setText(null);
            precio.setText(null);
        }


    }//GEN-LAST:event_InsertarActionPerformed

    private void aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aceptarActionPerformed
        JFileChooser selArchivo = new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter(".csv", "csv");
        selArchivo.setFileFilter(filtro);

        int seleccionar = selArchivo.showOpenDialog(this);

        if (seleccionar == JFileChooser.APPROVE_OPTION) {
            File archivo = selArchivo.getSelectedFile();
            guardarArchivo(archivo);

            PantallaDatos abrir = new PantallaDatos();
            abrir.setVisible(true);
            this.setVisible(false);
        }

    }//GEN-LAST:event_aceptarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Agregar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Agregar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Agregar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Agregar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Agregar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Insertar;
    private javax.swing.JButton aceptar;
    private javax.swing.JTextField clave;
    private javax.swing.JLabel fondo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField nombre;
    private javax.swing.JLabel obligar1;
    private javax.swing.JLabel obligar2;
    private javax.swing.JLabel obligar3;
    private javax.swing.JTextField precio;
    // End of variables declaration//GEN-END:variables
}